﻿In this era of communication digitization search is a very common process as it helps us to complete our tasks easily and 
quickly. Whether we are buying something or visit a social site such as Facebook and Instagram we expect to look for a 
search box somewhere on the website which will help you find what you are looking for without scanning the entire site. 
With time the user's requirement has gone a drastic change and so the search process has been updated with time and 
achieved a high standard of sophistication.
                                            To know about search in detail let's know about two different kinds of search 
for strings feild which are Keyword search  and full-text-search. In the case of keyword search entire string is considered as one keyword. The string is considered as one single unit and indexed as one. In the event of matching concerning keyword string, the entire string is matched.No partial match is allowed. On another hand, full-text search is modified and sophisticated. Over here the string is tokenized based on the analyzer and every token is indexed. In the case of 
the full-text-search partial match can be achieved as every tokenized string is indexed. Full-text-search-based search the engine is like google. Full-text search supports natural language querying.  
                                                                   As the technique for search operation changes with time, 
the relevance of search engines also evolved with time. In the case of the early engine, the result contains all the search terms than only the query as successful. In the case of the second generation not only matching documents but also adding a relevance score about the other document, inside in the database, better for research queries, rather for lookup. Now not only a word but a collection of words were provided and with the advent of the Modern search engine, more emphasis is given on high performance With huge document sets, moved on to find the one with the correct document.
                                       The paradigm has been completely shifted from exact result to the more appropriate 
data set or document that provide the answer. Now to understand how we are going to find which document  is more relevant we need to understand the few terms such as term frequency(T.F), Document frequency, Inverse document frequency(IDF). Term frequency is measured by how often does a term appears in the field of the document. It is calculated by the frequency of a particular text in the document.
         Formula: 
                 tf(t,d) = count of t in d/number of words.The document frequency is measured by the number of the document 
 in which the particular word is present. The occurrence of the term present in the document is measured by document frequency = occurrence of the word t in a document. Inverse document frequency as the name suggests is the inverse of the document frequency. We know that some words may have more presence but little importance. So to separate such words from
rare ones IDF came into operation. More is the IDF less is the relevance
Formula: IDF = log[(number of Document/Number of Document containing the word)]
                                                   The most trending search engine uses the concept of Elastic search.
Elastic search is an open-source search engine built on top of Apache Lucene is a java library. It is a free and open search engine software library supported by Apache Software Foundation. The elastic search uses NoSQL which gives him an extra edge over the traditional database. In case The traditional database uses a table to store the entities whereas in elastic search we use JSON which is created in a very simple human-readable form. As for example
let information = {
                  name :"X",
                  email:x@y.com
                  }

In object, things are present in key value pair, value which makes it very lucrative. Elastic search is distributed document store. So Once the document is stored in any part of the node, it can be retrieved from any part as
every position is self indexed. In Elastic search one of the most important technologies used are clusters which enable the multiple nodes to store terabytes of data.The important operation such as  searching and indexing are carried with help of clustering. The clustering nodes that receive the request will transfer the request to other nodes and the same way 
response is given back to the request. The node, cluster formation, node roles, Replication. 
                                          Cluster state as the name suggests holds the information of the state such as 
which are the indices in the cluster and the data mapping among them. The master node is the selected node in a particular cluster that helps us to coordinate among the other nodes in the cluster. Cluster formation is a kind of record  based on which clusters are formed and which is generally stored in the elasticsearch.yml config file. Node roles are the operation provide to every node for the efficient course of the task. Replication is veryimportant among the nodes of the cluster so that data can be shared by the various  node. 
                                     Indexing is another important process of Elastic search. It is the process of 
inserting a document into elastic-search. The important tool used by Elastic search is Logstash and Kibana. Logstash is an open-source, server-side data processing pipeline that takes the data from multiple sources simultaneously and transfers it to one of the stashes. Kibana is another tool that is used in elastic search. Kibana is open-source analytic and visualization platform designed to work with elastic search.
                                         Kibana with its visual medium helps us to interact with data stored in
elastic search indices. We can easily perform advanced data analysis and visualization in a variety of charts.
Example code for elastic search for indexing
{ "title" : "Introduction", "content": "Elasticsearch",

  "published\\_date" : "2021-07-08",

"tags":["elasticsearch","distributed","storage"] }

                                 We take the data in JSON format. To index our element in elastic search we will PUT 
request with syntax :
                    PUT/<index>/doc/\<id> 
                    
                    where doc is document we want to send  
                    
                    id is the document_id.

Requested output will be 

                   { 
                    "index" : "posts", 
                   "type" : "doc", 
                    "_id" : "1",
                   "Version" : 1, 
                    "result" : "created", 
                    "sharads" : { "total" : 2,
                    "successful" : 1, 
                    "failed" : 0, },
                      "shards" : {
                       "total" : 2,
                      "successful" : 1,
                      "failed" : 0
                    },
                "seq_no" : 0,
                "primary_term" : 1
                }

          We can see in the output result is created which means indexing is done.Over here the index,type,\id, version 
 is metadata.Now we will create a Get request to fetch the data .Syntax for Get request is Get /{index}/{type}/{id}
 GET Request
 GET /posts/_doc/1 // get request
{ "index" : "posts", 
"type" : "doc", 
"id" : "1", 
"Version" : 2,
"seq_no" : 1, 
"primaryterm" : 1, 
"found" : true, 
"source" : {
"title" : "Introduction", 
"content": "Elasticsearch", 
"published_date": "2021-07-08", 
"tags":[ "elasticsearch", "distributed", "storage" ]
  }
}

We found the document as the value of found is true.
